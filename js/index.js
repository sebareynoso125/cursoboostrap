$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 6000
    });

    $('#suscripcion').on('show.bs.modal', function(e){
      console.log('El modal se está mostrando ...');

      $('#suscripcionBtn').removeClass('btn-otline-success');
      $('#suscripcionBtn').addClass('btn-success');
      $('#suscripcionBtn').prop('disabled', true);

    });

    $('#suscripcion').on('shown.bs.modal', function(e){
      console.log('El modal se mostró');
    });

    $('#suscripcion').on('hide.bs.modal', function(e){
      console.log('El modal se está ocultando ...');
    });

    $('#suscripcion').on('hidden.bs.modal', function(e){
      console.log('El modal se ocultó');

      $('#suscripcionBtn').removeClass('btn-success');
      $('#suscripcionBtn').addClass('btn-otline-success');
      $('#suscripcionBtn').prop('disabled', false);
    });
  });